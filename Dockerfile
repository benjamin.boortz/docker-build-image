FROM docker:stable



#
# add files to container
#
ADD ./requirements.txt .



#
# install software
RUN \
	echo "*** updating syste ***" && \
	apk update && apk upgrade && \
	echo "*** installing system software ***" && \
	apk add --update \
		bash \
		docker \
		git \
		make \
		python \
		py-pip && \
	echo "### installing python related software" && \
	pip install -I -U pip && \
	pip install -I -U setuptools && \
	pip install -I -r requirements.txt && \
	rm -rf /var/cache/apk/* /tmp/*



#
# running tests
#
RUN \
	echo "### running simple tests" && \
	docker --version && \
	git --version && \
	make --version && \
	python --version && \
	pip --version && \
	gitchangelog --version
