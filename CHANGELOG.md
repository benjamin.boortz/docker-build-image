# Changelog


## 0.2.3 (2018-11-03)

### Changes

* Check if CHANGELOG.md has changed before we are going to commit. [Benjamin Boortz]


## 0.2.2 (2018-11-03)

### Changes

* Check if CHANGELOG.md has changed before we are going to commit. [Benjamin Boortz]

### Fix

* CHANGELOG.md update fixed. [Benjamin Boortz]


## 0.2.1 (2018-11-03)

### Changes

* Gitchangelog template changed. [Benjamin Boortz]

* Gitchangelog template changed. [Benjamin Boortz]

* Gitchangelog template changed. [Benjamin Boortz]


## 0.1.0 (2018-11-03)

### Other

* Add LICENSE. [Benjamin Boortz]

* Initial commit. [Benjamin Boortz]


