# docker-build-image

This container is containing software to build and test software in docker container.

# status

![Build Status](https://gitlab.com/benjamin.boortz/docker-build-image/badges/master/build.svg)
![Test Coverage](https://gitlab.com/benjamin.boortz/docker-build-image/badges/master/coverage.svg?job=coverage)


# How to build?

```
make docker_build
make test
make docker_push
```


# How to contribute?

You want to contribute to this code base in any way? Fixing typos, bugs, etc.
Simply fork this git repository, make your changes and create a pull request.
