#!/bin/bash

set -e
set -u



if [ "$GIT_RELEASE" == "true" ]; then
	CI_REPOSITORY_URL="https://$GIT_ACCESS_USER:$GIT_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git"

	echo "update repository with latest changes ..."
	git pull $CI_REPOSITORY_URL

	echo "generating CHANGELOG.md ..."
	gitchangelog > CHANGELOG.md

	# check if a commit is needed
	git diff --exit-code CHANGELOG.md && exit 0 || echo "CHANGELOG.md must be updated"

	echo "committing and pushing updates to git"
	git config user.email "$GIT_USER_EMAIL"
	git config user.name "Runner = $CI_RUNNER_DESCRIPTION"
	git config push.default matching
	git commit -m "chg: log: automatic CHANGELOG.md update using pipeline" CHANGELOG.md
	git push $CI_REPOSITORY_URL HEAD:$CI_COMMIT_REF_NAME
else
	echo "Variable GIT_RELEASE set to false. Therefore no CHANGELOG.md update needed."
fi

