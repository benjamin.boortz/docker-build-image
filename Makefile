version = v0.2
CI_COMMIT_REF_SLUG ?= -
timestamp = $(shell date +'%Y-%m-%d-%H-%M-%S')
tag = $(branch)-$(timestamp)
branch = $(CI_COMMIT_REF_SLUG)
ifeq (-,$(CI_COMMIT_REF_SLUG))
	branch = $(shell git rev-parse --abbrev-ref HEAD)
	tag = $(branch)-$(timestamp)
endif
ifeq (master,$(CI_COMMIT_REF_SLUG))
	tag = $(version)-$(timestamp)
endif
image = registry.gitlab.com/benjamin.boortz/docker-build-image:$(tag)
curdir = $(shell pwd)


cleanup:
	docker system prune --all --force

docker_build:
	docker build -t $(image) .

docker_push:
	docker build -t $(image) .
	docker push $(image)

changelog_update:
	ls -la
	$(curdir)/update_changelog.sh

debug:
	echo "image: $(image)"

test:
	docker build -t $(image) .
	docker run -i $(image) echo test
